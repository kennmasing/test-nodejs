import 'reflect-metadata';
import connectToMongo from 'core/loaders/connectToMongo';
import loadInitialData from 'core/loaders/loadInitialData';
import buildGqlSchema from 'core/loaders/buildGqlSchema';
import startZuittExpress from 'core/loaders/startZuittExpress';

(async (): Promise<void> => {
  await connectToMongo();
  await loadInitialData();
  const gqlSchema = await buildGqlSchema();
  startZuittExpress(gqlSchema);
})();