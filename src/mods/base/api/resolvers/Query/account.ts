import { UserInputError } from 'apollo-server-express';
import {
  Query, Resolver, Arg, ID,
} from 'type-graphql';
import { MAccount } from 'mods/base/db';
import Account from '../../entities/Account';

@Resolver()
export default class {
  @Query(() => Account)
  async account(
  @Arg('_id', () => ID) _id: string,
  ) {
    const account = await MAccount.findById(_id).lean();

    if (!account) {
      throw new UserInputError('User not found');
    }

    return account;
  }
}
