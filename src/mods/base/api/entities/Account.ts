import { Field, ID, ObjectType } from 'type-graphql';
import TimeStamps from './TimeStamps';
import Node from './Node';

@ObjectType({ implements: [Node] })
export default class Account extends TimeStamps {
  @Field(() =>ID)
  _id: string;

  @Field()
  firstName: string;

  @Field()
  lastName: string;
}