import { Field, InterfaceType } from 'type-graphql';
import { GraphQLDateTime } from 'graphql-iso-date';

@InterfaceType()
export default abstract class TimeStamps {
  @Field(() => GraphQLDateTime)
  createdAt: Date;

  @Field(() => GraphQLDateTime)
  updatedAt: Date;
}
