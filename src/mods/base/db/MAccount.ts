import { Schema, model } from 'mongoose';
import { Account } from './_types';

const AccountSchema =  new Schema(
  {
    firstName: String,
    lastName: String,
  },
  {
    collection: 'Account',
    timestamps: true,
  },
);

const MAccount = model<Account>('Account', AccountSchema);

export default MAccount;