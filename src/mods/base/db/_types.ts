import { Types, Document } from 'mongoose';

export interface UserStampedDocument {
  createdBy?: Types.ObjectId;
  updatedBy?: Types.ObjectId;
}

export interface Account extends Document, UserStampedDocument {
  firstName: string;
  lastName: string;
}