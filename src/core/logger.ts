import bunyan from 'bunyan';

const logger = bunyan.createLogger({
  name: 'test-nodejs',
})

export default logger;