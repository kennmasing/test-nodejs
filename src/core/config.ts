import envalid, { cleanEnv, str, email, json, port } from 'envalid';

const config = cleanEnv(process.env, {
  MONGODB_URI: str({ default: 'mongodb://localhost:27017/test-nodejs' }),
  NODE_ENV: str({ default: 'development' }),
  PORT: port({ default: 4000 }),
});

export default config;
