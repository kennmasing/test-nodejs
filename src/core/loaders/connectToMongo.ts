import mongoose from 'mongoose';
import config from '../config';

/* eslint-disable @typescript-eslint/no-unsafe-member-access,@typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-floating-promises */
export default async function connectToMongo(): Promise<void> {
  mongoose.set('debug', config.NODE_ENV === 'development');
  await new Promise((resolve) => {
    mongoose.connect(
      config.MONGODB_URI,
      {
        useFindAndModify: false,
        useCreateIndex: true,
        useUnifiedTopology: true,
        useNewUrlParser: true,
      },
      resolve,
    );
    mongoose.connection.once('open', () => {
      console.log('Now connected to local database')
    })
  });
}
