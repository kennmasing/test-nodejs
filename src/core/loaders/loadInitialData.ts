import { Types } from 'mongoose';
import { MAccount } from 'mods/base/db';

export default async function loadInitialData() {
  const adminFirstName = 'Kenneth';
  const adminLastName = 'Masing';

  const adminAccount = {
    _id: Types.ObjectId('607300bc2a616b09a8add4fd'),
    firstName: adminFirstName,
    lastName: adminLastName,
  };

  const adminAccountDoc = await MAccount.findOne({ _id: adminAccount._id });
  if (!adminAccountDoc) {
    await new MAccount(adminAccount).save();
  }
}
