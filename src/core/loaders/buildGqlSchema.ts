import { GraphQLSchema } from "graphql";
import { buildSchema } from 'type-graphql';
import baseResolvers from 'mods/base/api/resolvers';
import ping from "mods/base/api/resolvers/Query/ping";

export default async function buildGqlSchema(): Promise<GraphQLSchema> {
  const resolvers = [
    ...baseResolvers,
  ];

  const gqlSchema = await buildSchema({
    resolvers: [ping, ...resolvers],
  });
  return gqlSchema;
}