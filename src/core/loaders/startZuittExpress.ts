import { ApolloServer } from 'apollo-server-express';
import { GraphQLSchema } from 'graphql';
import cors from 'cors';
import express from 'express';
import http from 'http';
import logger from '../logger';
import config from 'core/config';
import { graphqlHTTP } from 'express-graphql';
import { graphqlUploadExpress } from 'graphql-upload';

export default function startZuittExpress(gqlSchema: GraphQLSchema): void {
  const apollo = new ApolloServer({
    uploads: false,
    formatError: (error) => {
      if (process.env.NODE_ENV !== 'production') {
        logger.error(error);
      }
      return error;
    },
    schema: gqlSchema,    
  })
  const app = express();
  app.use(cors());
  app.use(express.json({ limit: '2mb' }));
  app.get('/', (req, res) => {
    res.send('HELLO TESTING')
  })
  // app.use('/graphql', graphqlHTTP ({
  //   schema: gqlSchema,
  //   graphiql: true,
  // }));
  // HAVING SCHEMA FETCHING ISSUE WITH INSOMNIA (NOT YET RESOLVED )
  app.use(
    './graphql',
    graphqlUploadExpress({ maxFileSize: 10000000, maxFiles: 10 }),
  );
  apollo.applyMiddleware({ app });
  const httpServer = http.createServer(app);
  apollo.installSubscriptionHandlers(httpServer);

  console.log('---APOLLO---', apollo.graphqlPath)

  app.listen(config.PORT, () => {
    console.log(`🚀 Server ready at http://localhost:${config.PORT}${apollo.graphqlPath}`)
  });
}