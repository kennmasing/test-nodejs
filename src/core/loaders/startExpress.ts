import { ApolloServer } from 'apollo-server-express';
import { GraphQLSchema } from 'graphql';
import cors from 'cors';
import express from 'express';
import http from 'http';
import logger from '../logger';
import config from 'core/config';

export default function startExpress(gqlSchema: GraphQLSchema): void {
  const apollo = new ApolloServer({
    uploads: false,
    formatError: (error) => {
      if (process.env.NODE_ENV !== 'production') {
        logger.error(error);
      }
      return error;
    },
    schema: gqlSchema,
  })
  const app = express();
  app.use(cors());
  app.use(express.json({ limit: '2mb' }));
  app.use(
    './graphql',
  );
  apollo.applyMiddleware({ app });
  const httpServer = http.createServer(app);
  
  httpServer.listen({ port: config.PORT }, () => logger.info(
    `🚀 Server ready at http://localhost:${config.PORT}${apollo.graphqlPath}`
  ))
};
