"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const type_graphql_1 = require("type-graphql");
const resolvers_1 = __importDefault(require("../../mods/base/api/resolvers"));
const ping_1 = __importDefault(require("../../mods/base/api/resolvers/Query/ping"));
async function buildGqlSchema() {
    const resolvers = [
        ...resolvers_1.default,
    ];
    const gqlSchema = await type_graphql_1.buildSchema({
        resolvers: [ping_1.default, ...resolvers],
    });
    return gqlSchema;
}
exports.default = buildGqlSchema;
