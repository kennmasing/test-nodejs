"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const apollo_server_express_1 = require("apollo-server-express");
const cors_1 = __importDefault(require("cors"));
const express_1 = __importDefault(require("express"));
const http_1 = __importDefault(require("http"));
const logger_1 = __importDefault(require("../logger"));
const config_1 = __importDefault(require("../config"));
function startExpress(gqlSchema) {
    const apollo = new apollo_server_express_1.ApolloServer({
        uploads: false,
        formatError: (error) => {
            if (process.env.NODE_ENV !== 'production') {
                logger_1.default.error(error);
            }
            return error;
        },
        schema: gqlSchema,
    });
    const app = express_1.default();
    app.use(cors_1.default());
    app.use(express_1.default.json({ limit: '2mb' }));
    app.use('./graphql');
    apollo.applyMiddleware({ app });
    const httpServer = http_1.default.createServer(app);
    httpServer.listen({ port: config_1.default.PORT }, () => logger_1.default.info(`🚀 Server ready at http://localhost:${config_1.default.PORT}${apollo.graphqlPath}`));
}
exports.default = startExpress;
;
