"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("reflect-metadata");
const connectToMongo_1 = __importDefault(require("./core/loaders/connectToMongo"));
const loadInitialData_1 = __importDefault(require("./core/loaders/loadInitialData"));
const buildGqlSchema_1 = __importDefault(require("./core/loaders/buildGqlSchema"));
const startZuittExpress_1 = __importDefault(require("./core/loaders/startZuittExpress"));
(async () => {
    await connectToMongo_1.default();
    await loadInitialData_1.default();
    const gqlSchema = await buildGqlSchema_1.default();
    startZuittExpress_1.default(gqlSchema);
})();
