"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const AccountSchema = new mongoose_1.Schema({
    firstName: String,
    lastName: String,
}, {
    collection: 'Account',
    timestamps: true,
});
const MAccount = mongoose_1.model('Account', AccountSchema);
exports.default = MAccount;
