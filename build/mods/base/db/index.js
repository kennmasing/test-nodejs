"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MAccount = void 0;
var MAccount_1 = require("./MAccount");
Object.defineProperty(exports, "MAccount", { enumerable: true, get: function () { return __importDefault(MAccount_1).default; } });
